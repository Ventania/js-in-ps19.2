let btn_ex = document.querySelector('.delete')
let btn_add = document.querySelector('#add-book button')
let book = document.querySelector('#add-book input')
let listao = document.querySelector('ul')

btn_add.addEventListener('click', (e) =>{
	e.preventDefault();
	let livro = document.createElement('li');
	livro.innerHTML = `<span class="name">${book.value}</span>\n<span class="delete">excluir</span>`;
	listao.appendChild(livro);
})

listao.addEventListener('click', e =>{
	if(e.target.className == 'delete'){
		e.target.parentElement.parentElement.removeChild(e.target.parentElement);
	}
	})
